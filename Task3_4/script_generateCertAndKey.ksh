#!/bin/sh

kubectl exec -it $(kubectl get pods | grep artifakt-wp- | awk '{print $1}') -- openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt
kubectl exec -it $(kubectl get pods | grep artifakt-wp- | awk '{print $1}') -- service apache2 reload
