#!/bin/sh
kubectl exec -it $(kubectl get pods | grep artifakt-wp- | awk '{print $1}') -- wp maintenance-mode activate --allow-root --path="/var/www/html"