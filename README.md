# ARTIFAKT TECHNICAL TEST DEVOPS

## Global presentation
My name is Aymeric Mathéossian and I work since 18 years in IT Technologies. I had roles in companies (Banking, Energy, Transport, Pharm like Project Manager, Product Owner, DEVOPS but also CTO or IT Department Manager.

## Technical Test
The goal of the technical test is to get a deployment of a Wordpress website deployed with IAC for the Technical Test of Artifakt company.

## Choices
I used AZURE for my test and a docker-compose file to build the wordpress image and push it to the registry (gitlab).

I decided to use volumes from the beginning to keep data of Wordpress and data of the Mysql db. 

Images I used are these ones:
- Wordpress:latest
- Mysql:5.7

## Description of the package
Each requested task has been split into different folders and can be deployed with information explained here.

To clear everything after each task, just use (based on K8S labels):
```./script_clearAll.ksh```

### Task2: provide a way to make a deployment of a full applicative stack
#### Description
This deployment will setup a fresh Wordpress stack based on the wordpress:latest image. You'll have to setup the website on your own after that, using the web interface.

I built 3 files:
- kustomization.yaml: this file contains secrets (for password storage)
- mysql-deployment.yaml: deploys 3 elements
>- The Service on port 3306
>- The PVC with a storage of 20Go
>- THe deployment of the Mysql image as a container with defined Variable

- wordpress-deployment.yaml: deploys 3 elements
-- The Service on port 80 (LoadBalancer to allow Internet Access)
-- The PVC with a storage of 20Go
-- THe deployment of the wordpress image as a container with defined Variable

#### Deployment
To deploy it on your kubernetes cluster, use the command:
``` kubectl apply -k ./ ```

And get you service ip adress with
``` kubectl get service ```

### Task3 & 4: provide a way to build an immutable Docker image and make a scripted way to interact with pods
#### Description
In this task, we wanted to have something immutable for the Docker image. As we saw previously, Docker containers are already immutable because they are based on a simple images and we don't make any modification on it, just using volumes. We could have put everything in the Wordpress container (source code) and yes, here, the container wouldn't be immutable.

The image is on read-only mode. As you can see in the wordpress-deployment.yaml file, the image used is:
registry.gitlab.com/aymeric.matheossian/artifaktdevops/amatheossian-devopsit:latest

All information related to the image creation are explain in the Word Document.

#### Deployment
To deploy it on your kubernetes cluster, use the command:
``` kubectl apply -k ./ ```

And get you service ip adress:
``` kubectl get service ```

#### Renew the certificate
To renew the certificate, you should use the image to rebuild it, push it to the registry and update the image with

#### Maintenance mode

To activate maintenance mode on wp pod:
``` ./maintenance_activate.ksh ```

To deactivate maintenance mode on wp pod:
``` ./maintenance_deactivate.ksh ```